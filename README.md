1) Register node stats with EKG.

``` haskell

import System.Metrics                    -- ekg-core
import System.Metrics.DistributedProcess -- distributed-process-ekg
import System.Remote.Monitoring.Statsd   -- ekg-statsd

main = do
  -- ... init node

  store <- newStore
  registerGcMetrics store
  registerLocalNodeMetrics node store

  forkStatsd
    defaultStatsdOptions { flushInterval = 15e3 }
    store

  runProcess node procMain
```

2) Discover monitor leaks in your code.

![screenshot](https://i.imgur.com/5AeBIGG.png "collected and rendered in datadog")
