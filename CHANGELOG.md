# Change Log

All notable changes to this project will be documented in this file.
This project adheres to [Semantic Versioning](http://semver.org/).

## [0.1.1.0] - 2016-03-10

### Changed

- Wrap metric sampling in a looped process to prevent trace spam.

## [0.1.0.0] - 2016-02-27

### Added

- registerLocalNodeMetrics

[0.1.1.0]: https://bitbucket.org/dpwiz/distributed-process-ekg/commits/tag/0.1.1.0
[0.1.0.0]: https://bitbucket.org/dpwiz/distributed-process-ekg/commits/tag/0.1.0.0

